/**
 * Created by jan_w on 05.10.2017.
 */
public class BankAccount {

    private int balance = 100;

    public int getBalance() {
        return balance;
    }

    public void withdraw(int amount){
        balance -= amount;
    }
}
