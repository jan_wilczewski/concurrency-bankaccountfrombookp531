/**
 * Created by jan_w on 05.10.2017.
 */
public class Main {

    public static void main(String[] args) {

        MonikaRobert zadanie = new MonikaRobert();
        Thread watek1 = new Thread(zadanie);
        Thread watek2 = new Thread(zadanie);
        watek1.setName("Robert");
        watek2.setName("Monika");
        watek1.start();
        watek2.start();

    }
}
