/**
 * Created by jan_w on 05.10.2017.
 */
public class MonikaRobert implements Runnable{

    private BankAccount account = new BankAccount();


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            takeCash(10);
            if (account.getBalance() < 0){
                System.out.println("Przekroczenie limitu!");
            }
        }
    }

    public synchronized void takeCash(int howMuch){
        if (account.getBalance() >= howMuch) {
            System.out.println(Thread.currentThread().getName() + " ma zamiar pobrać gotówkę.");
            try {
                System.out.println("Wątek " + Thread.currentThread().getName() + " zaraz zostanie uśpiony.");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Wątek " + Thread.currentThread().getName() + " obudził się.");
            account.withdraw(howMuch);
            System.out.println("Wątek " + Thread.currentThread().getName() + " zakończył operację.");
        }
        System.out.println("Przykro mi, brak środków dla wątku: " + Thread.currentThread().getName());
    }
}
